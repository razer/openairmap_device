#ifndef _Const_H_
#define _Const_H_

// Comment the line on normal conditions
// #define DEBUG 1

// Device identification
#define FIRMWARE_NAME "OAMDEVICE"
#define FIRMWARE_VERSION "0.99"

#ifdef ESP32
#define DEVICE "ESP32"
#endif

#ifndef ESP32
#define DEVICE "ESP16"
#endif

// Pins
#define SWDOWN 19
#define SWUP 26
#define SWOK 18
#define GPS_POWER 5
#define DISPLAY_CLK 23
#define DISPLAY_SDA 16
#define LED 2
#define NOVA_TX 21
#define NOVA_RX 17
#define GPS_RX 27
#define GPS_TX 22

// Switch / LED
#define SWITCH_READ_FREQ_MS 100
#define LED_BLINK_FREQ_MS 20

// Get data
#define GETDATA_FREQ 30

// Display
#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
#define LCD_REFRESH_FREQ 1
#define LCD_DISPLAY_TIMEOUT 10

// Nova sensor
#define NOVA_WAKEUP_DELAY 30

// Gps
#define GPS_READ_FREQ 1
#define GPS_TIMEOUT 180

// Usb <-> Serial
#define SERIAL_READ_FREQ_MS 250
#define RECV_END_CHAR '\r'
const String CONFIRM_HANSHAKE = "OK";
const String ID_SEPARATOR = ":";
const String DATA_SPLIT_CHAR =  "|";
const String DATA_END_CHAR = "&";
const String ID_REQUEST =  "ID";
const String FETCH_REQUEST = "FT";
const String CLEARMEM_REQUEST = "CR";
const String CONFIG_REQUEST = "CF";
const uint8_t DATA_FREQ_MAP[5] = {1, 3, 5, 10, 60};
const uint8_t GPS_TIMEOUT_MAP[5] = {1, 2, 3, 5, 10};

// EEPROM
#ifdef ESP32
#define EEPROM_SIZE 11168
#endif

#ifndef ESP32
#define EEPROM_SIZE 8192
#endif

#define MAX_CONFIG_SIZE 32
// #define DATA_SIZE 14

#endif /* _Const_H_ */
