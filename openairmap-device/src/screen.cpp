#include <screen.h>

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

uint16_t draw_text(String text, uint8_t size,
                   uint8_t style, uint8_t align, int16_t y_pos) {
  int16_t  x1, y1;
  uint8_t bg_y_offset = 0;
  uint8_t txt_x_offset = 0;
  uint16_t text_width, text_height;
  switch (size) {
    case 5:
     display.setFont(&DroidSans5pt7b);
     // bg_y_offset += 1;
     if (style == INVERSE) txt_x_offset += 1;
     break;
    case 6: display.setFont(&DroidSans6pt7b); break;
    case 7: display.setFont(&DroidSans7pt7b); break;
  }
  display.getTextBounds(text, 0, 0, &x1, &y1, &text_width, &text_height);
  switch (align) {
    case A_LEFT: x1 = 0; break;
    case A_RIGHT: x1 = SCREEN_WIDTH - text_width - 2; break;
    case A_CENTER: x1 = (SCREEN_WIDTH - text_width) / 2; break;
  }
  y1 = y_pos + size + 4;
  display.setCursor(x1 + txt_x_offset, y1);
  if (style == INVERSE) {
    display.setTextColor(BLACK);
    display.fillRect(0, y_pos - bg_y_offset, SCREEN_WIDTH, y1 + 4 - y_pos, WHITE);
  } else {
    display.setTextColor(style);
  }
  display.print(text);
  return y1 + 2;
}

uint16_t draw_text(uint8_t text, uint8_t size,
                   uint8_t style, uint8_t align, int16_t y_pos) {
  return draw_text(String(text), size, style, align, y_pos);
}

void config_page(Config cfg, State state) {
  display.clearDisplay();
  String sleep_mode = cfg.sleep_mode ? "Screen" : "Off";
  if (cfg.sleep_mode == 2) sleep_mode += "+GPS";
  String title = (state.menu_index >= 30) ? "Config menu" : "Config update";
  draw_text(title, 7, WHITE, A_CENTER, 0);
  uint16_t last_height = draw_text(
    "Sleep mode:", 5, (state.menu_index == 30) ? INVERSE:WHITE, A_LEFT, 17);
  draw_text(sleep_mode, 5, (state.menu_index == 30) ? BLACK:WHITE, A_RIGHT, 17);
  draw_text("Data freq (min):", 5, (state.menu_index == 31) ? INVERSE:WHITE, A_LEFT,
            last_height);
  last_height = draw_text(cfg.data_freq, 5,
                          (state.menu_index == 31) ? BLACK:WHITE, A_RIGHT, last_height);
  draw_text("GPS timeout (min):", 5, (state.menu_index == 32) ? INVERSE:WHITE, A_LEFT,
            last_height);
  last_height = draw_text(cfg.gps_timeout, 5,
                          (state.menu_index == 32) ? BLACK:WHITE, A_RIGHT, last_height);
  if (state.menu_index >= 30)
    draw_text(state.config_updated ? "Save" : "Back", 5,
              (state.menu_index == 33) ? INVERSE:WHITE, A_LEFT, last_height);
  display.display();
}

void idle_page(Data data, Config cfg, SavedState saved_state, State state) {
  display.clearDisplay();
  String clock_str = (year() < 2018) ? format_date(day(), month()) + " "
                                       + format_time(hour(), minute(), second())
                                       : "No clock";
  draw_text((year() > 2018) ? format_clock() : "No clock", 7, WHITE, A_LEFT, 0);
  draw_text(format_mem_stats(saved_state), 5, WHITE, A_RIGHT, 0);
  draw_text("Next fetch:", 6, WHITE, A_LEFT, 22);
  uint16_t next_fetch = (cfg.data_freq * 60) - (seconds() - state.last_data_fetch);
  uint16_t last_height = draw_text(String(next_fetch) + "/" + (cfg.data_freq * 60),
                                   6, WHITE, A_RIGHT, 22);
  last_height += 2;
  draw_text("Last fetch:", 6, WHITE, A_LEFT, last_height);
  if (data.p10) {
    last_height = draw_text(String(data.p25) + "/" + String(data.p10), 6, WHITE, A_RIGHT,
                            last_height);
    if (data.lat > 0) {
      draw_text("Lat:" + String(data.lat,3) + "/Lng:" + String(data.lng,3), 5, WHITE,
                A_RIGHT, last_height);
    } else draw_text("No location", 5, WHITE, A_RIGHT, last_height);
  } else {
    draw_text("No data", 6, WHITE, A_RIGHT, last_height);
  }
  display.display();
}


void fetch_page(Data data, Config cfg, SavedState saved_state,
                State state) {
  display.clearDisplay();
  draw_text("Data fetch", 7, WHITE, A_LEFT, 0);
  draw_text(format_mem_stats(saved_state), 5, WHITE, A_RIGHT, 0);
  draw_text("Clock:", 5, WHITE, A_LEFT, 18);
  uint16_t last_height = draw_text((year() > 2018) ? format_clock() : "Wait...", 6,
                                   WHITE, A_RIGHT, 17) + 5;
  String counter = String(seconds() - state.fetch_start);
  if (data.p10) {
    draw_text(String(data.p25) + "/" + String(data.p10), 6, WHITE, A_RIGHT, last_height);
  } else {
    draw_text("Wait " + counter + "/" + String(cfg.nova_wakeup_delay), 6, WHITE,
              A_RIGHT, last_height);
  }
  last_height = draw_text("Pm2.5/Pm10:", 5, WHITE, A_LEFT, last_height) + 5;
  if (data.lat > 0) {
    draw_text("Lat:" + String(data.lat, 3) + " Lng:" + String(data.lng, 3), 6, WHITE,
              A_CENTER, last_height);
  } else if (state.on_fetch) {
    draw_text("Wait " + counter + "/" + String(cfg.gps_timeout * 60), 6, WHITE,
              A_RIGHT, last_height);
    last_height = draw_text("Location:", 5, WHITE, A_LEFT, last_height);
  }
  display.display();
}

void menu_page(Config cfg, State state) {
  display.clearDisplay();
  draw_text("Main menu", 7, WHITE, A_CENTER, 0);
  uint16_t last_height = draw_text(state.on_fetch ? "Cancel fetch" : "Fetch data", 6,
                                   (state.menu_index == 0) ? INVERSE:WHITE, A_CENTER, 17);
  last_height = draw_text("Enable Bluetooth", 6,
                          (state.menu_index == 1) ? INVERSE:WHITE, A_CENTER, last_height);
  last_height = draw_text("Config", 6,
                          (state.menu_index == 2) ? INVERSE:WHITE, A_CENTER, last_height);
  last_height = draw_text("Back", 6,
                          (state.menu_index == 3) ? INVERSE:WHITE, A_CENTER, last_height);

  display.display();
}

void eeprom_error_page() {
  draw_text("!!! Storage Error !!!", 7, WHITE, A_CENTER, 0);
  display.display();
  delay(1000000);
}

uint8_t _set_display_on(State * state) {
  if (!state->display_onpower) {
    state->display_onpower = true;
    display.ssd1306_command(SSD1306_DISPLAYON);
  }
  return true;
}

uint8_t set_display_power(Config cfg, State * state) {
  if (state->config_updated || (state->menu_index != -1))
    return _set_display_on(state);
  if (state->on_fetch && (seconds() - state->last_fetch_update < LCD_DISPLAY_TIMEOUT))
    return _set_display_on(state);
  if (seconds() - state->last_data_fetch < LCD_DISPLAY_TIMEOUT)
    return _set_display_on(state);
  if (seconds() - state->last_switchs_action < LCD_DISPLAY_TIMEOUT)
    return _set_display_on(state);
  if (state->display_onpower && (cfg.sleep_mode != 0)) {
    state->display_onpower = false;
    display.ssd1306_command(SSD1306_DISPLAYOFF);
  }
  return (cfg.sleep_mode == 0);
}

void init_display(State * state) {
  Wire.begin(DISPLAY_SDA, DISPLAY_CLK);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();
  display.display();
  display.setTextSize(1);
  display.setTextColor(BLACK, WHITE);
  state->display_onpower = true;
}
