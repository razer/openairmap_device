#ifndef _Utils_H_
#define _Utils_H_

#include "Arduino.h"
#include "Time.h"
#include "const.h"
#include "struct.h"

String format_time(int hour, int mins);
String format_time(int hour, int mins, int sec);
String format_date(int day, int mon);
String format_date(int day, int mon, int year);
String format_clock();
String format_clock(uint8_t show_year, uint8_t show_seconds);
String format_mem_stats(SavedState saved_state);
uint16_t get_next_map_value(const uint8_t * map, uint16_t value);
uint16_t seconds();

#endif /* _Utils_H_ */
