#ifndef _Screen_H_
#define _Screen_H_

#include "Arduino.h"
#include "Time.h"
#include "SPI.h"
#include "Wire.h"
#include "Adafruit_GFX.h"
#include "Adafruit_SSD1306.h"
#include "DroidSans5.h"
#include "DroidSans6.h"
#include "DroidSans7.h"

#include "const.h"
#include "struct.h"
#include "utils.h"

#define A_LEFT 0
#define A_RIGHT 1
#define A_CENTER 2

void init_display(State * state);
void fetch_page(Data data, Config cfg, SavedState saved_state, State state);
void idle_page(Data data, Config cfg, SavedState saved_state, State state);
void menu_page(Config cfg, State state);
void config_page(Config cfg, State state);
uint8_t set_display_power(Config cfg, State * state);
void eeprom_error_page();
#endif /* _Screen_H_ */
