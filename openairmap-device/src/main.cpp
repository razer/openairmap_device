#include <main.h>

#ifdef ESP32
HardwareSerial gps_serial(1);
HardwareSerial nova_serial(2);
#endif

#ifndef ESP32
SoftwareSerial gps_serial(GPS_RX, GPS_TX);
#endif

TinyGPSPlus gps;
SDS011 nova_sensor;
Config cfg;
Data data;
State state;
SavedState saved_state;
Led led;

void refresh_screen() {
  if (state.menu_index != -1) {
    if (seconds() - state.last_switchs_action > 10) state.menu_index = -1;
    return;
  }
  if (seconds() - state.last_lcd_refresh < LCD_REFRESH_FREQ) return;
  state.last_lcd_refresh = seconds();
  if (!set_display_power(cfg, &state)) return;
  if (state.config_updated) {
    config_page(cfg, state);
    state.config_updated = (seconds() - state.last_config_update < 5);
  } else if (state.on_fetch) fetch_page(data, cfg, saved_state, state);
  else idle_page(data, cfg, saved_state, state);
}

void read_novapm() {
  if ((!state.on_fetch) || state.valid_pm) return;
  // Wake up sensor
  if (!state.last_nova_wakeup) {
    state.last_nova_wakeup = seconds();
    nova_sensor.wakeup();
    state.nova_onpower = true;
    state.last_fetch_update = seconds();
    return;
  }
  // Wait for module to wake up
  if (seconds() - state.last_nova_wakeup < NOVA_WAKEUP_DELAY)
    return;
  // Finaly read data
  state.last_fetch_update = seconds();
  state.last_nova_wakeup = 0;
  nova_sensor.read(&data.p25, &data.p10);
  state.valid_pm = (data.p25 > 0);
  nova_sensor.sleep();
  state.nova_onpower = false;
}

void set_gps_power(uint8_t power_state) {
  if (cfg.sleep_mode < 2) return;
  digitalWrite(GPS_POWER, power_state);
  state.gps_onpower = power_state;
}

void update_time() {
  if (!state.gps_date_update) return;
  if ((year() < 2018) || (seconds() - state.last_time_update > 300)) {
    // state.last_fetch_update = seconds();
    state.gps_date_update = false;
    setTime(state.gps_hour, state.gps_mins, state.gps_sec, state.gps_day, state.gps_mon,
      state.gps_year);
  }
}

void read_gps() {
  if (!state.gps_onpower) return;
  if (!gps_serial.available()) return;
  while (gps_serial.available() > 0) {
    if (gps.encode(gps_serial.read())) {
      if (gps.location.isUpdated()) {
        state.last_fetch_update = seconds();
        data.lat = gps.location.lat();
        data.lng = gps.location.lng();
      }
      if (gps.date.isUpdated() & (gps.date.year() > 2018)) {
        state.gps_day = gps.date.day();
        state.gps_mon = gps.date.month();
        state.gps_year = gps.date.year();
        state.gps_date_update = true;
      }
      if (gps.time.isUpdated()) {
        state.gps_hour = gps.time.hour();
        state.gps_mins = gps.time.minute();
        state.gps_sec = gps.time.second();
      }
    }
  }

}

void read_switchs() {
  if (millis() - state.last_switch_read < SWITCH_READ_FREQ_MS) return;
  // uint8_t sw_state = (!digitalRead(SWDOWN)) | digitalRead(SWUP)<<1 | digitalRead(SWOK)<<2;
  uint8_t sw_state = (!digitalRead(SWDOWN)) | (!digitalRead(SWUP))<<1
                     | (!digitalRead(SWOK))<<2;
  state.last_switch_read = millis();
  if (sw_state == state.switchs) return;
  state.switchs = sw_state;
  state.last_switchs_action = seconds();
  if (!state.switchs) return;
  if (!state.display_onpower) {
    if (state.on_fetch) state.last_fetch_update = seconds();
    // else state.last_data_fetch = seconds();
    set_display_power(cfg, &state);
    return;
  }
  switch (state.switchs) {
    case 0x04:
      // Ok pressed
      if (state.menu_index == -1) {
        state.menu_index = 0;
        menu_page(cfg, state);
      } else if (state.menu_index == 0) {
        // Fetch data or cancel fetching
        if (state.on_fetch) {
          state.on_fetch = false;
          set_gps_power(false);
          nova_sensor.sleep();
          state.nova_onpower = false;
          state.last_data_fetch = seconds();
        } else state.user_fetch_request = true;
        state.menu_index = -1;
        refresh_screen();
      } else if (state.menu_index == 1) {
        // Bluetooth
        state.menu_index = -1;
        refresh_screen();
      } else if (state.menu_index == 2) {
        // Config
        state.menu_index = 30;
        config_page(cfg, state);
        break;
      } else if (state.menu_index == 3) {
        // Back
        state.menu_index = -1;
        refresh_screen();
      // Config page
      } else if (state.menu_index == 30) {
        cfg.sleep_mode = (cfg.sleep_mode == 2) ? 0 : cfg.sleep_mode + 1;
      } else if (state.menu_index == 31) {
        cfg.data_freq = get_next_map_value(DATA_FREQ_MAP, cfg.data_freq);
      } else if (state.menu_index == 32) {
        cfg.gps_timeout = get_next_map_value(GPS_TIMEOUT_MAP, cfg.gps_timeout);
      } else if (state.menu_index == 33) {
        // Save | Back
        if (state.config_updated) {
          cfg.nova_wakeup_delay = NOVA_WAKEUP_DELAY;
          EEPROM.put(sizeof(saved_state), cfg);
          EEPROM.commit();
          state.config_updated = false;
        }
        state.menu_index = 3;
        menu_page(cfg, state);
      }
      if ((state.menu_index >= 30) & (state.menu_index <= 32)) {
        state.config_updated = true;
        config_page(cfg, state);
      }
      break;
    case 0x01:
      // down
      if (state.menu_index == -1) return;
      if (state.menu_index >= 30) {
        state.menu_index = (state.menu_index == 33) ? 30 : state.menu_index+1;
        config_page(cfg, state);
      } else {
        state.menu_index = (state.menu_index == 3) ? 0 : state.menu_index+1;
        menu_page(cfg, state);
      }
      break;
    case 0x02:
      // up
      if (state.menu_index == -1) return;
      if (state.menu_index >= 30) {
        state.menu_index = (state.menu_index == 30) ? 33 : state.menu_index-1;
        config_page(cfg, state);
      } else {
        state.menu_index = (state.menu_index == 0) ? 3 : state.menu_index-1;
        menu_page(cfg, state);
      }
      break;
  }
}

void read_serial() {
  if (millis() - state.last_serial_read < SERIAL_READ_FREQ_MS) return;
  state.last_serial_read = millis();
  if (!Serial.available()) return;
  String cmd = Serial.readStringUntil(RECV_END_CHAR);
  if (cmd.startsWith(ID_REQUEST)) {
    // identification request
    Serial.print(ID_REQUEST+ID_SEPARATOR+FIRMWARE_NAME+DATA_SPLIT_CHAR);
    Serial.print(FIRMWARE_VERSION+DATA_SPLIT_CHAR+DEVICE+DATA_SPLIT_CHAR);
    Serial.println(saved_state.data_count);
  } else if (cmd.startsWith(FETCH_REQUEST)) {
    // Upload data
    Serial.print(FETCH_REQUEST+ID_SEPARATOR);
    uint8_t data_size = sizeof(data);
    for (uint16_t idx=0; idx<saved_state.data_count; idx++) {
      Data eeprom_data;
      EEPROM.get(MAX_CONFIG_SIZE+(idx*data_size), eeprom_data);
      Serial.print(eeprom_data.timestamp);
      Serial.print(DATA_SPLIT_CHAR); Serial.print(eeprom_data.p25);
      Serial.print(DATA_SPLIT_CHAR); Serial.print(eeprom_data.p10);
      Serial.print(DATA_SPLIT_CHAR); Serial.print(eeprom_data.lat, 4);
      Serial.print(DATA_SPLIT_CHAR); Serial.print(eeprom_data.lng, 4);
      Serial.print(DATA_END_CHAR);
    }
    Serial.println();
  }
  else if (cmd.startsWith(CLEARMEM_REQUEST)) {
    // Clear eeprom from data
    saved_state.eeprom_ptr = MAX_CONFIG_SIZE;
    saved_state.data_count = 0;
    EEPROM.put(0, saved_state);
    EEPROM.commit();
    Serial.println(CLEARMEM_REQUEST+ID_SEPARATOR+CONFIRM_HANSHAKE);
  }
  else if (cmd.startsWith(CONFIG_REQUEST)) {
    Serial.print(CONFIG_REQUEST+ID_SEPARATOR);
    if (cmd.length() > 7) {
      // Config update
      state.last_config_update = seconds();
      state.config_updated = true;
      cfg.sleep_mode = cmd.substring(3,4).toInt();
      cfg.data_freq = cmd.substring(4,6).toInt();
      cfg.gps_timeout = cmd.substring(6,8).toInt();
      cfg.nova_wakeup_delay = NOVA_WAKEUP_DELAY;
      EEPROM.put(sizeof(saved_state), cfg);
      EEPROM.commit();
      // write_eeprom_config(&EEPROM, cfg);
      Serial.println(CONFIRM_HANSHAKE);
    } else {
      // config report
      String data_freq = String(cfg.data_freq);
      if (data_freq.length() == 1) data_freq = "0" + data_freq;
      String gps_timeout = String(cfg.gps_timeout);
      if (gps_timeout.length() == 1) gps_timeout = "0" + gps_timeout;
      Serial.println(cfg.sleep_mode+data_freq+gps_timeout);
    }
  }
}

void fetch_new_data() {
  if (state.on_fetch)  {
    uint8_t timeout = (seconds() - state.fetch_start > cfg.gps_timeout*60);
    if ( (state.valid_pm & (data.lat>0)) | timeout ) {
      state.last_data_fetch = seconds();
      state.on_fetch = false;
      set_gps_power(false);
      // nova_sensor.sleep();
      // state.nova_onpower = false;
      if ((year() <= 2018) | !state.valid_pm) return;
      // Store new data to eeprom
      data.timestamp = now();
      EEPROM.put(saved_state.eeprom_ptr, data);
      if (saved_state.eeprom_ptr >= EEPROM_SIZE)
        // End of eeprom, erase oldest data
        saved_state.eeprom_ptr = MAX_CONFIG_SIZE;
      else {
        saved_state.eeprom_ptr += sizeof(data);
        saved_state.data_count++;
      }
      EEPROM.put(0, saved_state);
      EEPROM.commit();
    }
    return;
  }
  if (state.user_fetch_request)
    state.user_fetch_request = false;
  else
    if (seconds() - state.last_data_fetch < cfg.data_freq*60) return;
  state.fetch_start = seconds();
  state.gps_date_update = false;
  state.valid_pm = false;
  data.p25 = 0; data.p10 = 0;
  data.lat = 0; data.lng = 0;
  state.on_fetch = true;
  set_gps_power(true);
}

void blink_led() {
  if (millis() - led.last_blink < LED_BLINK_FREQ_MS) return;
  led.last_blink = millis();
  if (led.state) {
    digitalWrite(LED, LOW);
    led.state = false;
    return;
  }
  if (led.blink_count > (state.on_fetch ? 10 : 200)) {
    led.blink_count = 0;
    led.state = true;
    digitalWrite(LED, HIGH);
    return;
  }
  led.blink_count++;
}

#ifdef DEBUG
void debug_report() {
  if (seconds() - last_debug_report < 3) return;
  last_debug_report = seconds();
  Serial.println("-*-*-*-*-*-*-*");
  Serial.println("menu_index:" + String(state.menu_index));
  Serial.println("on_fetch:" + String(state.on_fetch));
  Serial.println("display_onpower:" + String(state.display_onpower));
  Serial.println("gps_date_update:" + String(state.gps_date_update));
  Serial.println("gps_onpower:" + String(state.gps_onpower));
  Serial.println("nova_onpower:" + String(state.nova_onpower));
  Serial.println("last_data_fetch:" + String(state.last_data_fetch));
  Serial.println("last_time_update:" + String(state.last_time_update));
  Serial.println("last_fetch_update:" + String(state.last_fetch_update));
}
#endif

void setup() {
  Serial.begin(115200);
  init_display(&state);
#ifdef ESP32
  if (!EEPROM.begin(EEPROM_SIZE))
    eeprom_error_page();
#endif
  EEPROM.get(0, saved_state);
  if (saved_state.data_count > 60000) {
    // No data in eeprom -> use default values
    saved_state.data_count = 0;
    saved_state.eeprom_ptr = MAX_CONFIG_SIZE;
  }
  EEPROM.get(sizeof(saved_state), cfg);
  pinMode(SWDOWN, INPUT_PULLUP);
  pinMode(SWUP, INPUT_PULLUP);
  pinMode(SWOK, INPUT_PULLUP);
  pinMode(LED, OUTPUT);
  pinMode(GPS_POWER, OUTPUT);
  digitalWrite(GPS_POWER, (cfg.sleep_mode != 2));
  state.gps_onpower = (cfg.sleep_mode != 2);
#ifdef ESP32
  gps_serial.begin(9600, SERIAL_8N1, GPS_RX, GPS_TX);
  nova_serial.begin(9600, SERIAL_8N1, NOVA_RX, NOVA_TX);
  nova_sensor.begin(&nova_serial);
#endif
#ifndef ESP32
  gps_serial.begin(9600);
  nova_sensor.begin(NOVA_RX, NOVA_TX);
#endif
  state.menu_index = -1;
  // Force fetching new data
  state.user_fetch_request = true;
  // Force display config values
  state.config_updated = true;
  state.gps_date_update = false;
}

void loop() {
  read_switchs();
  blink_led();
  fetch_new_data();
  read_serial();
  read_gps();
  read_novapm();
  refresh_screen();
  update_time();
#ifdef DEBUG
  debug_report();
#endif
}
