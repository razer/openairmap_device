#ifndef _Struct_H_
#define _Struct_H_

#include <Arduino.h>

typedef struct {
  uint16_t eeprom_ptr;
  uint16_t data_count;
} SavedState;

typedef struct {
  uint8_t sleep_mode;
  uint8_t nova_wakeup_delay;
  uint8_t gps_timeout;
  uint8_t data_freq;
  int8_t timezone;
} Config;

typedef struct {
  uint32_t timestamp;
  float p10;
  float p25;
  float lat;
  float lng;
} Data;

typedef struct {
  uint8_t gps_day;
  uint8_t gps_mon;
  uint8_t gps_hour;
  uint8_t gps_mins;
  uint8_t gps_sec;
  uint16_t gps_year;
  uint8_t gps_date_update;
  uint8_t config_updated;
  uint8_t on_fetch;
  uint8_t user_fetch_request;
  uint8_t display_onpower;
  uint8_t nova_onpower;
  uint8_t valid_pm;
  uint8_t switchs;
  uint16_t last_switchs_action;
  int8_t menu_index;
  uint8_t gps_onpower;
  uint16_t last_time_update;
  uint16_t last_data_fetch;
  uint16_t last_config_update;
  uint16_t last_lcd_refresh;
  uint16_t last_fetch_update;
  uint16_t last_nova_wakeup;
  uint16_t fetch_start;
  uint32_t last_switch_read;
  uint32_t last_serial_read;
} State;

typedef struct {
  uint8_t state;
  uint8_t blink_count;
  uint32_t last_blink;
} Led;


#endif /* _Struct_H_ */
