#include <utils.h>

String format_time(int hour, int mins) {
  String time_str;
  if (hour < 10) time_str = "0";
  time_str += String(hour) + ":";
  if (mins < 10) time_str += "0";
  return time_str + String(mins);
}

String format_time(int hour, int mins, int sec) {
  String time_str = format_time(hour, mins) + ":";
  if (sec < 10) time_str += "0";
  return time_str + String(sec);
}

String format_date(int day, int mon) {
  String date_str;
  if (day < 10) date_str = "0";
  date_str += String(day) + "/";
  if (mon < 10) date_str += "0";
  date_str += String(mon);
  return date_str;
}

String format_date(int day, int mon, int year) {
  return format_date(day, mon) + "/" + String(year);
}

String format_clock() {
  return format_clock(false, false);
}

String format_clock(uint8_t show_year, uint8_t show_seconds) {
  String date = show_year ? format_date(day(), month(), year())
                            : format_date(day(), month());
  String time = show_seconds ? format_time(hour(), minute(), second())
                            : format_time(hour(), minute());
  return date + " " + time;
}

String format_mem_stats(SavedState saved_state) {
  Data data;
  uint8_t mem_perc = saved_state.data_count * 100 / (EEPROM_SIZE / sizeof(data));
  return String(saved_state.data_count)+ "(" + String(mem_perc) + "%)";
}

uint16_t get_next_map_value(const uint8_t * map, uint16_t value) {
  uint8_t idx;
  for (idx=0; idx<sizeof(map); idx++) {
    if (map[idx] == value) break;
  }
  idx = (idx >= sizeof(map)) ? 0 : idx+1;
  return map[idx];
}

uint16_t seconds () {
  return (millis()/1000);
}
