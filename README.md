## Open Air Map Device
![Logo](https://framagit.org/razer/openairmap/raw/master/artwork/oam-icon.svg)

## In developpement - Use at your own risks

OpenAirMap provide a map with pollution data (actually pm2.5 and pm10 particules) coming from an ESP16/32 mobile device with NovaPm (or other dust sensor) and a GPS.

The GTK application have his own repo [here](https://framagit.org/razer/openairmap)

The device take dust values (pm2.5 and pm10 particules) with gps location at a user defined interval. Once connected to a computer, values are uploaded and can be shown in a map.

## Electronic Parts
- Wemos mini32
- 0.96 inch 128*64 i2c display module (ssd1306)
- Level converter module
- Neo6M GPS module
- NovaPm dust sensor (sds011)
- Lithium battery charging module with 5V Booster
- Led, switch and push buttons
- 3.5cm female mono jack
- 7x9cm prototype PCB Board
- 1000 mAh cylindric lithium ion battery

## Schematic and wiring
![Schematic](https://framagit.org/razer/openairmap_device/raw/master/schematic/oam_schematic.png)

## 3d parts for printing the case
[3d parts](https://framagit.org/razer/openairmap_device/tree/master/3d_parts) are available to print the case, with STL files and Freecad source files
![Case](https://framagit.org/razer/openairmap_device/raw/master/3d_parts/openairmap_case.png)

## Firmware
The [Firmware](https://framagit.org/razer/openairmap_device/tree/master/openairmap-device) is a platformio project
For using Arduino IDE, just rename main.cpp to main.ino..

The [const.h](https://framagit.org/razer/openairmap_device/blob/master/openairmap-device/src/const.h) file should be edited with pin definition.

Currently, only Expressif hardware is supported : ESP8266 or ESP32.

## Pictures
![Picture1](https://framagit.org/razer/openairmap_device/raw/master/pictures/oam-device_1.jpg)
![Picture2](https://framagit.org/razer/openairmap_device/raw/master/pictures/oam-device_2.jpg)
![Picture3](https://framagit.org/razer/openairmap_device/raw/master/pictures/oam-device_3.jpg)
![Picture4](https://framagit.org/razer/openairmap_device/raw/master/pictures/oam-device_4.jpg)
![Picture5](https://framagit.org/razer/openairmap_device/raw/master/pictures/oam-device_5.jpg)
![Picture6](https://framagit.org/razer/openairmap_device/raw/master/pictures/oam-device_6.jpg)
